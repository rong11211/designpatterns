package com.liurong.createpattern.builder;

import java.util.List;

public class BuilderTest {
	public static void main(String[] args) {
		Builder builder = new Builder();
		List<Sender> createEmailsSenders = builder.createEmailsSenders(10);
		for (Sender sender : createEmailsSenders) {
			sender.send();
		}
	}
}
