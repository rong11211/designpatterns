package com.liurong.createpattern.builder;

public interface Sender {
	public void send();
}
