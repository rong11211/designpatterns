package com.liurong.createpattern.builder;

import java.util.ArrayList;
import java.util.List;

/**
 * 创建者模式
 * @author liu.r
 *
 */
public class Builder {
	private List<Sender> senders = new ArrayList<Sender>();

	public  List<Sender> createSmsSenders(int count) {
		for (int i = 0; i < count; i++) {
			senders.add(new SmsSender());
		}
		return senders;
	}

	public List<Sender> createEmailsSenders(int count) {
		for (int i = 0; i < count; i++) {
			senders.add(new EmailsSender());
		}
		return senders;
	}
}
