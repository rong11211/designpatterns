package com.liurong.createpattern.singleton;

public class SingletonTest {
	public static void main(String[] args) {
		SingletonClass1 a1 = SingletonClass1.getInstance();
		SingletonClass1 a2 = SingletonClass1.getInstance();
		System.out.println(a1 == a2);
		
		SingletonClass2 b1 = SingletonClass2.getInstance();
		SingletonClass2 b2 = SingletonClass2.getInstance();
		System.out.println(b1 == b2);
		
		
		SingletonClass3 c1 = SingletonClass3.getInstance();
		SingletonClass3 c3 = SingletonClass3.getInstance();
		System.out.println(c1 == c3);
	}
}
