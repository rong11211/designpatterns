package com.liurong.createpattern.singleton;

/**
 * 懒汉式
 * 		因为每次调用getInstance()方法都用上锁，所以性能降低
 * @author liu.r
 *
 */
public class SingletonClass1 {
	private static SingletonClass1 instance = null;
	
	// 构造方法私有，不让创建事例
	private SingletonClass1() {}
	
	public static synchronized SingletonClass1 getInstance() {
		if (instance == null) {
			instance = new SingletonClass1();
		}
		return instance;
	}
}
