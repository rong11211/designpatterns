package com.liurong.createpattern.singleton;

/**
 * 双重锁机制 volatile和synchronized
 * 			jdk1.5才被赋予意义
 * @author liu.r
 *
 */
public class SingletonClass3 {
	public static volatile SingletonClass3 instance = null;

	// 构造方法私有，不让创建事例
	private SingletonClass3() {
	}

	public static SingletonClass3 getInstance() {
		if (instance == null) {
			synchronized (SingletonClass3.class) {
				if (instance == null) {
					instance = new SingletonClass3();
				}
			}
		}
		return instance;
	}
}
