package com.liurong.createpattern.singleton;

/**
 * 饿汉式
 * 		一旦访问该类的任何其他的静态域，就会造成实例的初始化，而事实是可能从始至终没有使用这个实例，造成内存的浪费
 * @author liu.r
 *
 */
public class SingletonClass2 {
	//
	public static final SingletonClass2 instance = new SingletonClass2();

	// 构造方法私有，不让创建事例
	private SingletonClass2() {
	}

	public static SingletonClass2 getInstance() {
		return instance;
	}
}
