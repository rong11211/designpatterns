package com.liurong.createpattern.singleton;

/**
 * 内部类的形式
 * 		推荐的形式
 * @author liu.r
 *
 */
public class SingletonClass4 {

	private SingletonClass4() {
	}

	public SingletonClass4 getInstance() {
		return SingletonInstance.instance;
	}
	
	private static class SingletonInstance {
		private static SingletonClass4 instance = new SingletonClass4();
	}
}
