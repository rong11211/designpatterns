package com.liurong.createpattern.abstractfactory;


public class SmsSenderFactory implements Provider {

	@Override
	public Sender createSender() {
		return new SmsSender();
	}
}
