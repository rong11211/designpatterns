package com.liurong.createpattern.abstractfactory;


public interface Provider {
	public Sender createSender();
}
