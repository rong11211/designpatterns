package com.liurong.createpattern.abstractfactory;

public interface Sender {
	public void send();
}
