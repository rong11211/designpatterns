package com.liurong.createpattern.abstractfactory;


/**
 * 抽象工厂模式
 * 使用范围：接口要求拓展性好的
 * 快速扩展：发及时信息，则只需做一个实现类，实现Sender接口，同时做一个工厂类，实现Provider接口
 * @author liu.r
 *
 */
public class EmailsSenderFactory implements Provider {

	@Override
	public Sender createSender() {
		return new EmailsSender();
	}
}
