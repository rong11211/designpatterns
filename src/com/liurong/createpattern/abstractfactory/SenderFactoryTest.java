package com.liurong.createpattern.abstractfactory;


public class SenderFactoryTest {
	public static void main(String[] args) {
		EmailsSenderFactory emailsSenderFactory = new EmailsSenderFactory();
		Sender sender1 = emailsSenderFactory.createSender();
		sender1.send();
	}
}
