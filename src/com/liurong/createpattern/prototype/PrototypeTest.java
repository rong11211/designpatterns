package com.liurong.createpattern.prototype;

import java.io.IOException;

public class PrototypeTest {
	public static void main(String[] args) throws ClassNotFoundException,
			IOException {
		Prototype prototype = new Prototype();
		prototype.setNum(10);
		int[] arr = { 10, 11, 12 };
		prototype.setArr(arr);
		prototype.setString("hello");

		System.out.println("---------浅复制---------------");
		System.out.println(prototype.getNum());
		System.out.println(prototype.getArr()[0]);
		System.out.println(prototype.getString());
		System.out.println("----------------------------");

		Prototype prototype2 = (Prototype) prototype.cloneShallow();
		prototype2.setNum(10);
		int[] arr2 = prototype2.getArr();
		arr2[0] = 201;
		prototype2.setArr(arr2);
		prototype2.setString("world");

		System.out.println(prototype.getNum());
		System.out.println(prototype.getArr()[0]);
		System.out.println(prototype.getString());
		System.out.println("----------------------------");

		System.out.println(prototype2.getNum());
		System.out.println(prototype2.getArr()[0]);
		System.out.println(prototype2.getString());
		System.out.println("----------------------------");

		System.out.println("---------深复制---------------");
		System.out.println(prototype.getNum());
		System.out.println(prototype.getArr()[0]);
		System.out.println(prototype.getString());
		System.out.println("----------------------------");

		Prototype prototype3 = (Prototype) prototype.cloneDeep();
		prototype3.setNum(10);
		int[] arr3 = prototype3.getArr();
		arr3[0] = 301;
		prototype3.setArr(arr3);
		prototype3.setString("world");

		System.out.println(prototype.getNum());
		System.out.println(prototype.getArr()[0]);
		System.out.println(prototype.getString());
		System.out.println("----------------------------");

		System.out.println(prototype3.getNum());
		System.out.println(prototype3.getArr()[0]);
		System.out.println(prototype3.getString());
		System.out.println("----------------------------");
	}
	
}
