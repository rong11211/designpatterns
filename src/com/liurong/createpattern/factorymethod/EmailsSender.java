package com.liurong.createpattern.factorymethod;


public class EmailsSender implements Sender {

	@Override
	public void send() {
		System.out.println("发送emails！");
	}
}
