package com.liurong.createpattern.factorymethod;



/**
 * 工厂方法模式（静态方法）
 * 适用范围：出现大量的实例需要创建，并且具有共同的接口时，可以通过工厂方法模式进行创建。
 * @author liu.r
 *
 */
public class SendFactory {
	
	public static Sender createEmailsSender() {
		return new EmailsSender();
	}
	
	public static Sender createSmsSender() {
		return new SmsSender();
	}
}
