package com.liurong.createpattern.factorymethod;



public class SendFactoryTest {
	public static void main(String[] args) {
		Sender sender1 = SendFactory.createEmailsSender();
		sender1.send();
		
		Sender sender2 = SendFactory.createSmsSender();
		sender2.send();
	}
}
