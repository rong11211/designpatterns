package com.liurong.createpattern.factorymethod;

public interface Sender {
	public void send();
}
