package com.liurong.architecturepattern.adapter.interfaceadapter;

public interface InterfaceMethod {
	void method1();
	void method2();
	void method3();
}
