package com.liurong.architecturepattern.adapter.interfaceadapter;

/**
 * 接口的适配器
 * @author liu.r
 *
 */
public class Method1 extends ImplementMethod {

	@Override
	public void method1() {
		System.out.println("method1....");
	}
}
