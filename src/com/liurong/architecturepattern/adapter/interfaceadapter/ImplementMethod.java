package com.liurong.architecturepattern.adapter.interfaceadapter;

public abstract class ImplementMethod implements InterfaceMethod {

	@Override
	public void method1() {
		
	}

	@Override
	public void method2() {
		
	}

	@Override
	public void method3() {
		
	}
}
