package com.liurong.architecturepattern.adapter.interfaceadapter;

/**
 * 接口的适配器
 * @author liu.r
 *
 */
public class Method2 extends ImplementMethod {

	@Override
	public void method2() {
		System.out.println("method2....");
	}
}
