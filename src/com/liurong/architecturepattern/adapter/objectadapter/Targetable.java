package com.liurong.architecturepattern.adapter.objectadapter;

public interface Targetable {
	void method1();
	void method2();
}
