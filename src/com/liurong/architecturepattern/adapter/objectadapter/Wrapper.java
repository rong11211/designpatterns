package com.liurong.architecturepattern.adapter.objectadapter;

/**
 * 对象的适配器模式
 * @author liu.r
 *
 */
public class Wrapper implements Targetable {
	private Source source;
	
	public Wrapper() {
	}
	public Wrapper(Source source) {
		super();
		this.source = source;
	}


	@Override
	public void method1() {
		source.method1();
	}

	@Override
	public void method2() {
		System.out.println("method2......");
	}
	
}
