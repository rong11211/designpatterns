package com.liurong.architecturepattern.adapter.classadapter;

public class ClassAdapterTest {
	public static void main(String[] args) {
		ClassAdapter classAdapter = new ClassAdapter();
		classAdapter.method1();
		classAdapter.method2();
	}
}
