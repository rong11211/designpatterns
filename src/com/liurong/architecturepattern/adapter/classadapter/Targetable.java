package com.liurong.architecturepattern.adapter.classadapter;

public interface Targetable {
	void method1();
	void method2();
}
