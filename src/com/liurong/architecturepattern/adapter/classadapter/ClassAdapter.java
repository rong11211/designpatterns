package com.liurong.architecturepattern.adapter.classadapter;

/**
 * 类的适配器
 * @author liu.r
 *
 */
public class ClassAdapter extends Source implements Targetable {

	@Override
	public void method2() {
		System.out.println("method2......");
	}

}
