package com.liurong.architecturepattern.facade;

public class Cpu {
	public void startup() {
		System.out.println("cpu startup!");
	}
	
	public void shutdown() {
		System.out.println("cup shutdown");
	}
}
