package com.liurong.architecturepattern.facade;

public class ComputerTest {
	public static void main(String[] args) {
		Computer computer = new Computer();
		computer.startup();
		
		System.out.println("-------------------------");
		computer.shutdown();
	}
}
