package com.liurong.architecturepattern.facade;

/**
 * 外观模式
 * 		该设计模式没有涉及到接口
 * @author liu.r
 *
 */
public class Computer {
	private Cpu cpu;
	private Disc disc;
	private Memory memory;
	public Computer() {
		super();
		cpu = new Cpu();
		disc = new Disc();
		memory = new Memory();
	}
	
	public void startup() {
		System.out.println("start computer!");
		cpu.startup();
		disc.startup();
		memory.startup();
		System.out.println("start computer success!");
	}
	
	public void shutdown() {
		System.out.println("shutdown computer!");
		cpu.shutdown();
		disc.shutdown();
		memory.shutdown();
		System.out.println("shutdown computer success!");
	}
}
