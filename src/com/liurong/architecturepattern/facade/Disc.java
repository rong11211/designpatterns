package com.liurong.architecturepattern.facade;

public class Disc {
	public void startup() {
		System.out.println("disc startup!");
	}
	public void shutdown() {
		System.out.println("disc shutdown!");
	}
}
