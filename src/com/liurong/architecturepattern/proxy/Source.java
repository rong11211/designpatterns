package com.liurong.architecturepattern.proxy;

public class Source implements Sourceable {

	@Override
	public void method1() {
		System.out.println("method1.....");
	}

}
