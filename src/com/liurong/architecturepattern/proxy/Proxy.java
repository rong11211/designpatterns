package com.liurong.architecturepattern.proxy;

public class Proxy implements Sourceable {
	private Source source;
	
	
	public Proxy() {
		super();
		source = new Source();
	}


	@Override
	public void method1() {
		this.before();
		source.method1();
		this.after();
	}


	private void after() {
		System.out.println("after....");
	}


	private void before() {
		System.out.println("before....");
	}
}
