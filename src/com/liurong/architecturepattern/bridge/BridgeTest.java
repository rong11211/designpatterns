package com.liurong.architecturepattern.bridge;

public class BridgeTest {
	public static void main(String[] args) {
		DriverManager driver = new DriverManager();
		driver.setDriver(new MysqlDriver());
		driver.getConnection();
		System.out.println("--------------");
		DriverManager driver2 = new DriverManager();
		driver2.setDriver(new OrcleDriver());
		driver2.getConnection();
	}
}
