package com.liurong.architecturepattern.bridge;

public interface Driver {
	void getConnection();
}
