package com.liurong.architecturepattern.bridge;

public class DriverManager implements Driver {
	private Driver driver;

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	@Override
	public void getConnection() {
		driver.getConnection();
	}
}
