package com.liurong.architecturepattern.decorator;

public class DecoratorTest {
	public static void main(String[] args) {
		Decorator decorator = new Decorator(new Source());
		decorator.method1();
	}
}
