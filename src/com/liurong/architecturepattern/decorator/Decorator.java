package com.liurong.architecturepattern.decorator;

/**
 * 装饰者模式
 * @author liu.r
 *
 */
public class Decorator implements Sourceable {
	private Source source;

	public Decorator(Source source) {
		super();
		this.source = source;
	}

	@Override
	public void method1() {
		System.out.println("装饰开始.....");
		source.method1();
		System.out.println("装饰结束.....");
	}
	
}
